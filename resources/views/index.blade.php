@extends('layout')

@section('content')
<table id="users-table" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Ciudad</th>
                <th>Teléfono</th>
                <th>Guardar</th> 
            </tr>
        </thead>
    </table>
@endsection

@section('scripts')
    <!-- Cargar DataTables con CDN -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    
    <!-- Cargar el archivo de traducción en español desde CDN -->
    <script src="https://cdn.datatables.net/plug-ins/1.11.5/i18n/Spanish.json"></script>

    <script>
        $(document).ready(function () {            
            $('#users-table').DataTable({
                data: @json($users),
                columns: [
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'email' },
                    { data: 'address.city' }, // Mostrar la ciudad desde la dirección del usuario
                    { data: 'phone' },
                    {
                        data: null,
                        render: function (data, type, row) {
                            return `
                                <form action="{{ route('usuarios.guardar') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="id" value="${row.id}">
                                    <button type="submit" class="btn btn-primary btn-save text-center">Guardar</button>
                                </form>
                            `;
                        }
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.13.5/i18n/es-ES.json" // Cargar el archivo de traducción en español desde CDN
                },
            });
        });
    </script>
@endsection
