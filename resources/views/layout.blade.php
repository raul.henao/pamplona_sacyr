<!DOCTYPE html>
<html>

<head>   

    <!-- Estilos de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <!-- Estilos personalizados para DataTable -->
    <link href="{{ asset('css/custom-datatable.css') }}" rel="stylesheet">

</head>

<body>
    <!-- Contenido de la vista -->
    @yield('content')

    <!-- Scripts -->
    @yield('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    {!! Toastr::message() !!}

</body>

</html>
