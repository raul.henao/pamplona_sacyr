<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $table = 'usuarios'; // Especifica el nombre de la tabla de la base de datos

    protected $fillable = ['nombre', 'correo', 'telefono', 'ciudad', 'contraseña']; // Agregar 'contraseña' aquí
}

   