<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;

class UserController extends Controller
{
    public function index()
    {
        // Método privado para obtener los usuarios desde la API externa
        $users = $this->getUsersFromExternalAPI();
        return view('index', compact('users'));
    }

    public function guardar(Request $request)
    {
        // Obtener el ID del usuario desde la solicitud
        $id = $request->input('id');

        // Obtener los usuarios desde la API externa
        $users = $this->getUsersFromExternalAPI();

        // Buscar el usuario por ID en la colección
        $usuarioEncontrado = collect($users)->firstWhere('id', $id);

        if ($usuarioEncontrado) {
            // Verificar si el correo electrónico ya existe en la base de datos local
            $correoExistente = DB::table('usuarios')->where('correo', $usuarioEncontrado->email)->exists();
            if ($correoExistente) {
                Toastr::error('El correo electrónico ya está registrado.', 'Error');
                return redirect()->back();
            }

            try {
                DB::beginTransaction();

                // Crear un nuevo objeto de Usuario
                $user = new Usuario();
                $user->nombre = $usuarioEncontrado->name;
                $user->correo = $usuarioEncontrado->email;
                $user->telefono = $usuarioEncontrado->phone;
                $user->ciudad = $usuarioEncontrado->address->city ?? null;
                $password = Str::random(10);
                $user->contraseña = Hash::make($password);
                $user->save();

                DB::commit();

                Toastr::success('Usuario guardado exitosamente', 'Éxito');
                return redirect()->back();
            } catch (\Exception $e) {
                DB::rollback();

                Toastr::error('Hubo un error al guardar el usuario', 'Error');
                return redirect()->back();
            }
        } else {
            Toastr::error('Usuario no encontrado', 'Error');
            return redirect()->back();
        }
    }

    // Método privado para obtener los usuarios desde la API externa
    private function getUsersFromExternalAPI()
    {
        $client = new Client();
        $response = $client->get('https://jsonplaceholder.typicode.com/users');
        $users = json_decode($response->getBody());
        return $users;
    }
}
